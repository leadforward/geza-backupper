import { app, BrowserWindow, ipcMain } from 'electron'
const fs = require('fs')
const path = require('path')

/**
 * Set `__statics` path to static files in production;
 * The reason we are setting it here is that the path needs to be evaluated at runtime
 */
if (process.env.PROD) {
  global.__statics = require('path').join(__dirname, 'statics').replace(/\\/g, '\\\\')
}

let mainWindow

function createWindow () {
  /**
   * Initial window options
   */
  mainWindow = new BrowserWindow({
    width: 1000,
    height: 600,
    useContentSize: true
  })

  mainWindow.loadURL(process.env.APP_URL)

  mainWindow.on('closed', () => {
    mainWindow = null
  })

  // mainWindow.webContents.on('will-navigate', (event) => event.preventDefault());
}

app.on('ready', createWindow)

app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit()
  }
})

app.on('activate', () => {
  if (mainWindow === null) {
    createWindow()
  }
})

// ipcMain.on('ondrop', (event, filePath) => {
ipcMain.on('ondrop', (event, filePath) => {
  console.log('fajl jon', filePath)
  let filesInDir = fs.readdirSync(path.dirname(filePath)).sort();
  event.sender.send('tartalmazo-mappa-elemei', filesInDir)
})
